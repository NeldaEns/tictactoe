using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController ins;
    public List<Text> buttonList;
    public Image playerXIcon;
    public Image playerOIcon;

    public Sprite tilePlayerX;
    public Sprite tilePlayerO;
    public Sprite tileEmpty;

    [Header("STRING")]
    public string playerSide;
    private string computerSide;

    public bool playerMove;
    public float delay;
    private int value;

    private int moveCount;

    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        playerSide = "X";
        computerSide = "O";
        SetPlayerColors(UIController.ins.playerX, UIController.ins.playerO);
        moveCount = 0;
        playerMove = true;
    }

    public string GetPlayerSide()
    {
        return playerSide;
    }

    public string GetComputerSide()
    {
        return computerSide;
    }


    public Sprite GetPlayerSprite()
    {
        if (playerSide == "X") return tilePlayerX;
        else return tilePlayerO;
    }

    public void EndTurn()
    {
        moveCount++;
        if (buttonList[0].text == playerSide && buttonList[1].text == playerSide && buttonList[2].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[3].text == playerSide && buttonList[4].text == playerSide && buttonList[5].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[6].text == playerSide && buttonList[7].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[0].text == playerSide && buttonList[3].text == playerSide && buttonList[6].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[1].text == playerSide && buttonList[4].text == playerSide && buttonList[7].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[2].text == playerSide && buttonList[5].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[0].text == playerSide && buttonList[4].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }

        else if (buttonList[2].text == playerSide && buttonList[4].text == playerSide && buttonList[6].text == playerSide)
        {
            GameOver(playerSide);
        }
        //--------------------------------------------------------------------

        else if (buttonList[0].text == computerSide && buttonList[1].text == computerSide && buttonList[2].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[3].text == computerSide && buttonList[4].text == computerSide && buttonList[5].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[6].text == computerSide && buttonList[7].text == computerSide && buttonList[8].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[0].text == computerSide && buttonList[3].text == computerSide && buttonList[6].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[1].text == computerSide && buttonList[4].text == computerSide && buttonList[7].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[2].text == computerSide && buttonList[5].text == computerSide && buttonList[8].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[0].text == computerSide && buttonList[4].text == computerSide && buttonList[8].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (buttonList[2].text == computerSide && buttonList[4].text == computerSide && buttonList[6].text == computerSide)
        {
            GameOver(computerSide);
        }

        else if (moveCount >= 9)
        {
            GameOver("draw");
        }

        else
        {
            ChangeSides();
            delay = 10;
        }
    }

    private void Update()
    {
        if(playerMove == false)
        {
            delay += delay * Time.deltaTime;
            if(delay >= 20)
            {
                value = Random.Range(0, 8);
                if(buttonList[value].GetComponentInParent<Button>().interactable == true)
                {
                    buttonList[value].text = GetComputerSide();
                    buttonList[value].GetComponentInParent<Button>().interactable = false;
                    EndTurn();
                }
            }
        }
    }

    public void SetPlayerColors(Player newPlayer, Player oldPlayer)
    {
        newPlayer.panel.color = UIController.ins.activePlayerColor.panelColor;
        newPlayer.text.color = UIController.ins.activePlayerColor.textColor;
        oldPlayer.panel.color = UIController.ins.inactivePlayerColor.panelColor;
        oldPlayer.text.color = UIController.ins.inactivePlayerColor.textColor;
    }


    public void ChangeSides()
    {
        //playerSide = (playerSide == "X") ? "O" : "X";
        playerMove = (playerMove == true) ? false : true;
        //if(playerSide =="X")
        if (playerMove == true)
        {
            SetPlayerColors(UIController.ins.playerX, UIController.ins.playerO);
        }
        else
        {
            SetPlayerColors(UIController.ins.playerO, UIController.ins.playerX);
        }
    }

    public void GameOver(string winningPlayer)
    {

        ToggleButtonState(false);
        if(winningPlayer == "draw")
        {
            SetGameOverText(" DRAW! "); 
        }
        else
        {
            SetGameOverText(winningPlayer + " Wins! ");
        }
        UIController.ins.gameOverPanel.SetActive(true);
        ToggleButtonState(false);
    }

    public void SetGameOverText(string value)
    {
        UIController.ins.gameOverPanel.SetActive(true);
        UIController.ins.gameOverTxt.text = value;
    }

    public void RestartGame()
    {
        playerSide = "X";
        moveCount = 0;
        UIController.ins.gameOverPanel.SetActive(false);
        ToggleButtonState(true);
        for (int i = 0; i < buttonList.Count; i++)
        {
            buttonList[i].text = "";
        }
    }

    private void ToggleButtonState(bool state)
    {
        for (int i = 0; i < buttonList.Count; i++)
        {
            buttonList[i].GetComponentInParent<Button>().interactable = state;
        }
    }

}
