using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridSpace : MonoBehaviour
{
    public Button button;
    public Text buttonText;

    public void SetSpace()
    {
        if (GameController.ins.playerMove)
        {
            buttonText.text = GameController.ins.GetPlayerSide();
            button.image.sprite = GameController.ins.GetPlayerSprite();
            button.interactable = false;
            GameController.ins.EndTurn();
        }
    }

}
